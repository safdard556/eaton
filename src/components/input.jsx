import { View, StyleSheet, TextInput, Text } from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { colors } from "../utils/theme";

/**
 *
 * inputTitle => will be the header of each input
 * isSecure => if you need a password alike field you use this
 * */

function Input({
  inputTitle,
  isSecure,
  error,
  errorMsg,
  valid,
  onChangeText,
  keyboardType,
  autoCapitalize
}) {
  return (
    <>
      <View
        style={[
          styles.container,
          error && { borderColor: colors.danger, borderWidth: 1 },
        ]}
      >
        <Text style={[styles.title, error && { color: colors.danger }]}>
          {inputTitle.toUpperCase()}
        </Text>

        <View style={styles.inputSubCon}>
          <TextInput
            onChangeText={onChangeText}
            style={styles.input}
            secureTextEntry={isSecure}
            keyboardType={keyboardType}
            autoCapitalize={autoCapitalize}
          />
          {error && <Ionicons name={"close"} color={colors.danger} size={20} />}
          {valid && (
            <Ionicons name={"checkmark"} color={colors.success} size={20} />
          )}
        </View>
      </View>

      {error && <Text style={styles.error}> {errorMsg} </Text>}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 60,
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
    justifyContent: "center",
    backgroundColor: colors.white,
  },

  inputSubCon: {
    flexDirection: "row",
    alignItems: "center",
  },

  input: {
    width: "95%",
  },

  title: {
    fontSize: 10,
    color: colors.secondary,
  },

  error: {
    fontSize: 10,
    color: colors.danger,
  },
});

export { Input };
