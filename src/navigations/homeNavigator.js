import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "@expo/vector-icons";
import { colors } from "../utils/theme";

import Home from "../screens/home";
const Tab = createBottomTabNavigator();

const iconsNamesOutlined = {
  Home: "home-outline",
  Raws: "cart-outline",
  Dishes: "basket-outline",
  Sales: "heart-outline",
  Profile: "person-outline",
};

const iconsNamesFilled = {
  Home: "home",
  Raws: "cart",
  Dishes: "basket",
  Sales: "heart",
  Profile: "person",
};

function HomeNavigator() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, focused }) => {
          // detecth each tab and customise its icon if needed
          let size = 22;
          let icon = "home";
          if (focused === true) {
            size = 33;
            icon = iconsNamesFilled[route.name];
          } else {
            icon = iconsNamesOutlined[route.name];
          }

          return <Ionicons name={icon} size={size} color={color} />;
        },
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: colors.secondary,
        headerShown: false,
      })}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Raws" component={Home} />
      <Tab.Screen name="Dishes" component={Home} />
      <Tab.Screen name="Sales" component={Home} />
      <Tab.Screen name="Profile" component={Home} />
    </Tab.Navigator>
  );
}

export { HomeNavigator };
