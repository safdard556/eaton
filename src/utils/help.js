import * as EmailValidator from "email-validator";

/***
 * a function that takes input from user
 * check if its valid email or not
 * sendds true or false
 */
const validateEmail = (value) => {
  const isValid = EmailValidator.validate(value);

  return isValid;
};

export { validateEmail };
