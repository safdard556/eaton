
import { View, StyleSheet, TouchableOpacity, Text } from "react-native";
import React from "react";
import { Header, Button, Input } from "../components";

export default function Home({ navigation }) {
  return (
    <View>
      <Header headingText={"Home"} />
      <View style={styles.form}>

        <View style={styles.buttonCon}>
          <Button
            text={"submit"}
            onBtnPress={() => {
              alert("hello");
            }}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonCon: {
    height: 45,
    width: "80%",
    alignSelf: "center",
    marginVertical: 10,
  },
  form: {
    padding: 10,
  },
  forgoP: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-end",
  },
  dontHaveAccount: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
  },
});
