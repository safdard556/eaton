import { View, StyleSheet, TouchableOpacity, Text } from "react-native";
import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {signInWithEmailAndPassword} from 'firebase/auth'

import { colors } from "../utils/theme";
import { Header, Button, Input } from "../components";
import { validateEmail } from "../utils/help";
import { auth } from "../services/db";


export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorPassword, setErrorPassword] = useState("");

  const onSignupPress = () => {
    navigation.navigate("Signup");
  };

  const onForgotPress = () => {
    navigation.navigate("ForgotPassword");
  };

  const onSubmit = () => {
    if (password && email) {
      if (validateEmail(email) === false) {
        setErrorEmail("email is not valid");
      } else if (email) {
        setErrorEmail("");
      }

      if (password.length < 6) {
        setErrorPassword("password is less than 6");
      } else if (password) {
        setErrorPassword("");
      }


      // if no erorr exist then just ok
      if(!errorPassword && !errorEmail){


        signInWithEmailAndPassword(auth,email,password).then(response=>{
            alert('user is logged in')
        }).catch(error=>{

            alert(error.message)
        })



      }




    }

    if (!password || !email) {
      if (email === "") {
        setErrorEmail("email is empty");
      } else if (validateEmail(email) === false) {
        setErrorEmail("email is not valid");
      } else if (email) {
        setErrorEmail("");
      }

      if (password === "") {
        setErrorPassword("password is not valid");
      } else if (password) {
        setErrorPassword("");
      }
    }
  };

  return (
    <View>
      <Header headingText={"Login"} />
      <View style={styles.form}>
        <Input
          inputTitle={"email"}
          onChangeText={setEmail}
          error={errorEmail}
          errorMsg={errorEmail}
          valid={!errorEmail}
          keyboardType={"email-address"}
          autoCapitalize={false}
        />
        <Input
          inputTitle={"password"}
          onChangeText={setPassword}
          isSecure={true}
          error={errorPassword}
          errorMsg={errorPassword}
          valid={!errorPassword}
        />

        <TouchableOpacity style={styles.forgoP} onPress={onForgotPress}>
          <Text>Forgot your password</Text>
          <Ionicons name={"arrow-forward"} color={colors.primary} />
        </TouchableOpacity>

        <View style={styles.buttonCon}>
          <Button text={"login"} onBtnPress={onSubmit} />
        </View>
        <TouchableOpacity
          style={styles.dontHaveAccount}
          onPress={onSignupPress}
        >
          <Text>Dont have an account signup</Text>
          <Ionicons name={"arrow-forward"} color={colors.primary} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonCon: {
    height: 45,
    width: "80%",
    alignSelf: "center",
    marginVertical: 10,
  },
  form: {
    padding: 10,
  },
  forgoP: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-end",
  },
  dontHaveAccount: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
  },
});
