import { View, StyleSheet, TouchableOpacity, Text } from "react-native";
import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { Dropdown } from "react-native-element-dropdown";

import { colors } from "../utils/theme";
import { Header, Button, Input } from "../components";
import { validateEmail } from "../utils/help";

const genderData = [
  { label: "Male", value: "Male" },
  { label: "Female", value: "Female" },
];

export default function Signup({ navigation }) {
  const [gender, setGender] = useState("");
  const [name, setName] = useState("");
  const [errorName, setErrorName] = useState("");
  const [email, setEmail] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorConfirmPass, setErrorConfirmPass] = useState("");

  const goBack = () => {
    navigation.goBack();
  };

  // when user tris to submit the form
  const onSubmit = () => {
    // variable === false   =>  !variable

    if (!name || !password || !email || !confirmPassword || !gender) {
      if (name === "") {
        setErrorName("name is not valid");
      } else if (name) {
        setErrorName("");
      }

      if (email === "") {
        setErrorEmail("email is empty");
      } else if (validateEmail(email) === false) {
        setErrorEmail("email is not valid");
      } else if (email) {
        setErrorEmail("");
      }

      if (password === "") {
        setErrorPassword("password is not valid");
      } else if (password) {
        setErrorPassword("");
      }

      if (confirmPassword === "") {
        setErrorConfirmPass("confirmPassword is not valid");
      } else if (confirmPassword) {
        setErrorConfirmPass("");
      }

      if (confirmPassword !== password) {
        setErrorConfirmPass("Password dont match");
        setErrorPassword("Password dont match");
      } else if (password && confirmPassword) {
        setErrorConfirmPass("");
        setErrorPassword("");
      }
    }
  };

  const handleGender = (item) => {
    setGender(item.value);
  };

  return (
    <View>
      <Header headingText={"Signup"} />
      <View style={styles.form}>
        <Input
          onChangeText={setName}
          inputTitle={"name"}
          error={errorName}
          errorMsg={errorName}
          valid={!errorName}
        />
        <Input
          onChangeText={setEmail}
          inputTitle={"email"}
          error={errorEmail}
          errorMsg={errorEmail}
          valid={!errorEmail}
        />
        <Input
          onChangeText={setPassword}
          inputTitle={"password"}
          isSecure={true}
          error={errorPassword}
          errorMsg={errorPassword}
          valid={!errorPassword}
        />
        <Input
          onChangeText={setConfirmPassword}
          inputTitle={"confirm password"}
          isSecure={true}
          error={errorConfirmPass}
          errorMsg={errorConfirmPass}
          valid={!errorConfirmPass}
        />

        <Dropdown
          data={genderData}
          labelField="label"
          valueField="value"
          placeholder={"Select Gender"}
          onChange={handleGender}
          value={gender}
        />

        <TouchableOpacity style={styles.alreadyAccount} onPress={goBack}>
          <Text>Already have an account</Text>
          <Ionicons name={"arrow-forward"} color={colors.primary} />
        </TouchableOpacity>

        <View style={styles.buttonCon}>
          <Button text={"Signup"} onBtnPress={onSubmit} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonCon: {
    height: 45,
    width: "80%",
    alignSelf: "center",
    marginVertical: 10,
  },
  form: {
    padding: 10,
  },
  alreadyAccount: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-end",
  },
});
