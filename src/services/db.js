import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBbdiXID1TI8MsPRgXX0R7-sy4z933OQmU",
  authDomain: "eaton-25c18.firebaseapp.com",
  projectId: "eaton-25c18",
  storageBucket: "eaton-25c18.appspot.com",
  messagingSenderId: "627749448052",
  appId: "1:627749448052:web:3e362767553e5ca0c12421",
  measurementId: "G-L8CX7GVTRH"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app)


export {auth};
